"use strict";

module.exports = {
	KEY_SECRET		: 'key',
	DATABASE_URL	: 'http://neo4j:123456aA@@localhost:7474',
	PORT			: 3000,
	HMAC_KEY		: 'ABCXYZ',
	AVATAR_DEFAULT	: '/avatars/default.jpg',
	CODE_EXPIRE_TIME: 86400000,
	CODE_ATTEMPT	: 3,
	userEmail		: "tuvichat@gmail.com",
	passwordEmail	: "123456aA@",
	FACEBOOK_LOGIN  : 1
};