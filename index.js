"use strict";

var common = require('./common.js'),
	accountManagement = require('./accountmanagement.js'),
	userInteraction = require('./userinteraction.js');

var hskey = common.fs.readFileSync('ssl/private.key');
var hscert = common.fs.readFileSync('ssl/certificate.crt');

var options = {
	key: hskey,
	cert: hscert
};

var app = common.express();
var port = common.config.PORT || 3000;

app.use(common.express.static('public'));
app.use(common.bodyParser.urlencoded({ extended: false }));
app.use(common.bodyParser.json());
app.use(common.morgan('dev'));

app.use(function(req, res, next){
	var params;

	if(Object.keys(req.body).length != 0){
		params = req.body;
	}else{
		params = req.query;
	}

	if(getToken(params) == params['_token']){
		next();
	}else{
		res.json({
			success: false,
			message: 'Token is invalid',
			token: getToken(req.body)
		});
	}
});

var routes = common.express.Router();

routes.post('/register', accountManagement.register);
routes.post('/login', accountManagement.login);
routes.post('/getCodeFromEmail', accountManagement.getCodeFromEmail);
routes.post('/resetPassword', accountManagement.resetPassword);

routes.post('/sendRequestFriend', userInteraction.sendRequestFriend);
routes.post('/acceptRequestFriend', userInteraction.acceptRequestFriend);
routes.post('/rejectRequestFriend', userInteraction.rejectRequestFriend);
routes.post('/deleteFriend', userInteraction.deleteFriend);
routes.post('/getListFriends', userInteraction.getListFriends);
routes.post('/getListRequestFriends', userInteraction.getListRequestFriends);

app.use('/api', routes);

common.https.createServer(options, app).listen(port);
console.log('Server is listening on port: '+port);

function getToken(params){
	var keys = Object.keys(params);
	var arrayParams = [];
	for(var i=0;i<keys.length;i++){
		if(keys[i] != '_token'){
			arrayParams.push(params[keys[i]]+"");
		}
	}
	arrayParams.sort();
	var temp = arrayParams.join(';') + common.config.KEY_SECRET;
	var token = common.blueimpMd5.md5(temp);
	return token;
}


