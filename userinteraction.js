"use strict";

var common = require('./common.js');

function sendRequestFriend(req, res){
	var id1 = req.body.id1;
	var id2 = req.body.id2;
	var query = [
		'MATCH (user1:User ), (user2:User)',
		'WHERE id(user1) = {props}.id1 AND id(user2) = {props}.id2',
		'MERGE (user1)-[r:REQUEST_FRIEND]->(user2)',
		'ON CREATE SET r.created_date = timestamp()',
		'RETURN r',
	].join('\n');

	var params = {
		props: {		
			id1: parseInt(id1),
			id2: parseInt(id2)
		}
	};

	common.db.cypher({
		query: query,
		params: params,
	}, function (err, results) {
		if(err){
			res.json({success: false});
		}else{
			res.json({success: true});
		}
	});
}

function acceptRequestFriend(req, res){
	var id1 = req.body.id1;
	var id2 = req.body.id2;
	var query = [
		'MATCH (user1:User), (user2:User)',
		'WHERE id(user1) = {props}.id1 AND id(user2) = {props}.id2',
		'MERGE (user1)-[f:FRIEND]-(user2)',
		'ON CREATE SET f.created_date = timestamp()',
		'WITH user1, user2',
		'MATCH (user1)-[r:REQUEST_FRIEND]-(user2)',
		'DELETE r'
	].join('\n');

	var params = {
		props: {		
			id1: parseInt(id1),
			id2: parseInt(id2)
		}
	};

	common.db.cypher({
		query: query,
		params: params,
	}, function (err, results) {
		if(err){
			res.json({success: false});
		}else{
			res.json({success: true});
		}
	});
}

function rejectRequestFriend(req, res){
	var id1 = req.body.id1;
	var id2 = req.body.id2;
	var query = [
		'MATCH (user1:User)-[r:REQUEST_FRIEND]-(user2:User)',
		'WHERE id(user1) = {props}.id1 AND id(user2) = {props}.id2',
		'DELETE r'
	].join('\n');

	var params = {
		props: {		
			id1: parseInt(id1),
			id2: parseInt(id2)
		}
	};

	common.db.cypher({
		query: query,
		params: params,
	}, function (err, results) {
		if(err){
			res.json({success: false});
		}else{
			res.json({success: true});
		}
	});
}

function deleteFriend(req, res){
	var id1 = req.body.id1;
	var id2 = req.body.id2;
	var query = [
		'MATCH (user1:User)-[r:FRIEND]-(user2:User)',
		'WHERE id(user1) = {props}.id1 AND id(user2) = {props}.id2',
		'DELETE r'
	].join('\n');

	var params = {
		props: {		
			id1: parseInt(id1),
			id2: parseInt(id2)
		}
	};

	common.db.cypher({
		query: query,
		params: params,
	}, function (err, results) {
		if(err){
			res.json({success: false});
		}else{
			res.json({success: true});
		}
	});
}

function getListFriends(req, res){
	var id = req.body.id;
	var query = [
		'MATCH (user:User)-[r:FRIEND]-(friend:User)',
		'WHERE id(user) = {props}.id',
		'RETURN id(friend) as id, friend.name as name, friend.avatar as avatar'
	].join('\n');

	var params = {
		props: {		
			id: parseInt(id)
		}
	};

	common.db.cypher({
		query: query,
		params: params,
	}, function (err, results) {
		if(err){
			res.json({success: false, message: "System error"});
		}else{
			var listFriends = results;
			res.json({success: true, listFriends});
		}
	});
}

function getListRequestFriends(req, res){
	var id = req.body.id;
	var query = [
		'MATCH (user:User)<-[r:REQUEST_FRIEND]-(request_friend:User)',
		'WHERE id(user) = {props}.id',
		'RETURN id(request_friend) as id, request_friend.name as name, request_friend.avatar as avatar'
	].join('\n');

	var params = {
		props: {		
			id: parseInt(id)
		}
	};

	common.db.cypher({
		query: query,
		params: params,
	}, function (err, results) {
		if(err){
			res.json({success: false, message: "System error"});
		}else{
			var listRequestFriends = results;
			res.json({success: true, listRequestFriends});
		}
	});
}

module.exports = {
		sendRequestFriend: sendRequestFriend,
		acceptRequestFriend: acceptRequestFriend,
		rejectRequestFriend: rejectRequestFriend,
		deleteFriend: deleteFriend,
		getListFriends: getListFriends,
		getListRequestFriends:getListRequestFriends
}