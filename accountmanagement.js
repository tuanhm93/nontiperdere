"use strict";

var common = require('./common.js');

var smtpTransport = common.nodemailer.createTransport("SMTP",{
	service: "Gmail",
	auth: {
		user: common.config.userEmail,
		pass: common.config.passwordEmail
	}
});


function register(req, res){
	var name = req.body.name || '';
	var email = req.body.email || '';
	var password = req.body.password || '';

	if((typeof name == "string") && common.validator.isEmail(email) && common.validator.isLength(password, 6) && (name = name.trim()).length != 0){
		var time = new Date().getTime();
		var query = [
			'CREATE (user:User:UserNormal {props})'	
		].join('\n');

		params = {
			props: {
				name: name,
				email:email,
				password: common.blueimpMd5.md5(password, common.config.HMAC_KEY),
				created_date:time,
				updated_date:time,
				avatar: common.config.AVATAR_DEFAULT,
				code: genCode(),
				code_expire_time: time,
				code_attempt: common.config.CODE_ATTEMPT
			}
		};
		common.db.cypher({
			query: query,
			params: params,
		}, function (err, results) {
			if(err){
				if(err.neo4j != undefined){
					res.json({success: false, message: "Email is already in use"});
				}else{
					res.json({success: false, message: "System error"});
				}
			}else{
				res.json({ success: true});
			}
		});
	}else{
		res.json({success: false, message: "There's something wrong with your input"});
	}
}

function login(req, res){
	if(req.body.type == common.config.FACEBOOK_LOGIN){
		var access_token = req.body.token;
		common.fb.setAccessToken(access_token);
		common.fb.api('/me',{ fields: ['id', 'name', 'picture'] }, function (resFb) {
			if(!resFb || resFb.error) {
				res.json({success: false, message: 'System error'});
				return;
			}

			var query = [
				'MERGE (user:UserFacebook:User {id: {props}.id})',
				'ON CREATE SET user = {props}',
				'ON MATCH SET user.token = {props}.token',
				'RETURN id(user) as id, user.name as name, user.avatar as avatar'
			].join('\n');

			var time = new Date().getTime();
			var params = {
				props: {
					id: resFb.id,
					name: resFb.name,
					avatar: resFb.picture.data.url,
					token: access_token,
					created_date: time,
					updated_date: time		
				}
			};
			common.db.cypher({
				query: query,
				params: params,
			}, function (err, results) {
				if(err){
					res.json({success: false, message: 'System error'});
				}else{
					res.json({success: true, user: {id: results[0].id, name: results[0].name, avatar: results[0].avatar}});
				}
			});
		});
	}else if(common.validator.isEmail(req.body.email)){
		var query = [
			'MATCH (user:UserNormal {email: {props}.email})',
			'RETURN id(user) as id, user.name as name, user.avatar as avatar, user.password as password',
		].join('\n');

		var params = {
			props: {		
				email: req.body.email		
			}
		};

		common.db.cypher({
			query: query,
			params: params,
		}, function (err, results) {
			if(err){
				res.json({success: false, message: "System error"});
			}else if(results.length == 0){
				res.json({success: false, message: "Email is not exist"});
			}else{
				if(results[0].password == common.blueimpMd5.md5(req.body.password, common.config.HMAC_KEY)){
					res.json({success: true, user: {id: results[0].id, name: results[0].name, avatar: results[0].avatar} });
				}else{
					res.json({success: false, message: "Password is incorrect"});
				}
			}
		});
	}else{
		res.json({success: false, message: "Email or password is wrong"});
	}
}

function getCodeFromEmail(req, res){
	var email = req.body.email;
	var code = genCode();
	var expireTime = new Date().getTime() + common.config.CODE_EXPIRE_TIME;

	var query = [
			'MATCH (user:UserNormal {email: {props}.email})',
			'SET user.code = {props}.code, user.code_attempt = 0, user.code_expire_time = {props}.expireTime',
			'RETURN id(user)',
	].join('\n');

	var params = {
		props: {		
			email: email,
			code: code,
			expireTime: expireTime
		}
	};

	common.db.cypher({
		query: query,
		params: params,
	}, function (err, results) {
		if(err){
			res.json({success: false, message: "System error"});
		}else if(results.length == 0){
			res.json({success: false, message: "Email is not exist"});
		}else{
			var mailOptions={
				to: email,
				subject: "[FORGOT PASSWORD]",
				text: "Your code securiry is: "+ code
			};
			smtpTransport.sendMail(mailOptions, function(err, inf){
				if(err){
					res.json({success: false, message: "System error"});
				}else{
					res.json({success: true});
				}
			});
		}
	});
}

function resetPassword(req, res){
	var email = req.body.email;
	var code = req.body.code;
	var password = req.body.password;
	var time = new Date().getTime();
	console.log(time);
	if(common.validator.isLength(password, 6)){
		var query = [
			'MATCH (user:UserNormal {email: {props}.email})',
			'RETURN user',
		].join('\n');

		var params = {
			props: {		
				email: email
			}
		};

		common.db.cypher({
			query: query,
			params: params,
		}, function (err, results) {
			if(err){
				res.json({success: false, message: "System error"});
			}else if(results.length == 0){
				res.json({success: false, message: "Email is not exist"});
			}else if(results[0].user.properties.code_attempt < common.config.CODE_ATTEMPT && results[0].user.properties.code_expire_time > time){
				if(results[0].user.properties.code == code){
					query = [
						'MATCH (user:UserNormal {email: {props}.email})',
						'SET user.password = {props}.password, user.updated_date = {props}.time, user.code_attempt = {props}.codeAttempt',
						'RETURN user'
					].join('\n');

					params = {
						props: {		
							email: email,
							password: common.blueimpMd5.md5(password, common.config.HMAC_KEY),
							time: time,
							codeAttempt: common.config.CODE_ATTEMPT
						}
					};
					common.db.cypher({
						query: query,
						params: params,
					}, function (err, results) {
						if(err){
							res.json({success: false, message: "System error"});
						}else{
							res.json({success: true});
						}
					});
				}else{
					res.json({success: false, message: "Code is incorrect"});
					query = [
						'MATCH (user:UserNormal {email: {props}.email})',
						'SET user.code_attempt = user.code_attempt + 1',
						'RETURN user'
					].join('\n');

					params = {
						props: {		
							email: email,
						}
					};
					common.db.cypher({
						query: query,
						params: params,
					}, function(err, results){

					});
				}
			}else{
				res.json({success: false, message: "Code hết hiệu lực, hoặc nhập sai quá nhiều!"});
			}
		});
	}else{
		res.json({success: false, message: "Password is too short"});
	}
}


function genCode(){
	code = "" + Math.ceil(Math.random()*9) + Math.ceil(Math.random()*9) + 
			    Math.ceil(Math.random()*9) + Math.ceil(Math.random()*9) + 
			    Math.ceil(Math.random()*9) + Math.ceil(Math.random()*9) ;
	return parseInt(code);
}

module.exports = {
	register: register,
	login: login,
	getCodeFromEmail: getCodeFromEmail,
	resetPassword: resetPassword
};