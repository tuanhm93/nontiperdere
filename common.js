"use strict";

var fs = require('fs'),
	config = require('./config.js'),
	neo4j = require('neo4j'),
	validator = require('validator'),
	blueimpMd5 = require('blueimp-md5'),
	db = new neo4j.GraphDatabase(config.DATABASE_URL),
	fb = require('fb'),
	nodemailer = require('nodemailer'),
	https = require('https'),
	express = require('express'),
	moment = require('moment'),
	morgan = require('morgan'),
	bodyParser = require('body-parser');

module.exports = {
	fs: fs,
	config: config,
	validator: validator,
	db: db,
	blueimpMd5: blueimpMd5,
	fb: fb,
	nodemailer: nodemailer,
	https: https,
	express: express,
	moment: moment,
	morgan: morgan,
	bodyParser: bodyParser
}
